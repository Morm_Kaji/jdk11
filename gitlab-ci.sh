# You can override the included template(s) by including variable overrides
# SAST customization: https://docs.gitlab.com/ee/user/application_security/sast/#customizing-the-sast-settings
# Secret Detection customization: https://docs.gitlab.com/ee/user/application_security/secret_detection/#customizing-settings
# Dependency Scanning customization: https://docs.gitlab.com/ee/user/application_security/dependency_scanning/#customizing-the-dependency-scanning-settings
# Note that environment variables can be set in several places
# See https://docs.gitlab.com/ee/ci/variables/#cicd-variable-precedence

sudo update-alternatives --install "/usr/bin/java" "java" "/usr/lib/jvm/jdk-11.0.12/bin/java" 0
sudo update-alternatives --install "/usr/bin/javac" "javac" "/usr/lib/jvm/jdk-11.0.12/bin/javac" 0

sudo update-alternatives --set java /usr/lib/jvm/jdk-11.0.12/bin/java
sudo update-alternatives --set javac /usr/lib/jvm/jdk-11.0.12/bin/javac

update-alternatives --list java
update-alternatives --list javac
